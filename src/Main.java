import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Rectangle a = new Rectangle(8,5 ,new Point(10,7));
        System.out.println("Area of the rectangle:"+ a.area());
        System.out.println("Perimeter of the rectangle:"+ a.perimeter());
        System.out.println(Arrays.deepToString(a.corners()));

        Circle b = new Circle(10,new Point(2 ,2));
        System.out.println("Area of the circle:"+b.area());
        System.out.println("Perimeter of the circle:"+b.perimeter());

        Circle c= new Circle(15,new Point(2 ,5));
        System.out.println(b.intersect(b,c));


    }
}
