public class Point {
    public int xCoord;
    public int yCoord;

    public  Point(int xCoord, int yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

}
